## O:

- Learned a lot useful annotations for testing during code review.
- Studied DTO and Mapper, understood how to handle data transferring from and to client properly.
- Introduced Flyway, a tool to conduct versions control of database which can be used in existed project without influencing current data. 
- H eld our first Retro and figured out many action to solve existing problems.
- Presented on Microservices, keep on interacting with audience during the speech.

## R:

- satisfied and pleasant.

## I:

- I felt pleasant since our presentation went well and got positive feedback from tutors and classmates
- Wonder how to handle useless or out dated Flyway scripts in project

## D:

- Keep interacting with the audience to keep them involved in the future sessions.
