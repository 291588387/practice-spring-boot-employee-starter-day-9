package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CompanyMapper {
    public static Company toEntity(CompanyRequest companyRequest){
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest, company);
        return company;
    }

    public static CompanyResponse toResponse(Company company) {
        CompanyResponse companyResponse= new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        companyResponse.setEmployeesCount(Objects.isNull(company.getEmployees()) ? 0 : company.getEmployees().size());
        return companyResponse;
    }

    public static List<CompanyResponse> toResponseList(List<Company> companyList) {
        return companyList.stream()
                .map(CompanyMapper::toResponse)
                .collect(Collectors.toList());
    }
}
