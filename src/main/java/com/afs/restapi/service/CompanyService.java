package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JpaCompanyRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final JpaCompanyRepository jpaCompanyRepository;

    public CompanyService(InMemoryCompanyRepository inMemoryCompanyRepository, JpaCompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    public List<CompanyResponse> findAll() {
        return CompanyMapper.toResponseList(jpaCompanyRepository.findAll());
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        return CompanyMapper.toResponseList(jpaCompanyRepository.findAll(PageRequest.of(page - 1, size)).getContent());
    }

    public CompanyResponse findById(Long id) {
        return CompanyMapper.toResponse(
                jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new));
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Optional<Company> optionalCompany = jpaCompanyRepository.findById(id);
        optionalCompany.ifPresent(previousCompany -> {
            previousCompany.setName(companyRequest.getName());
            jpaCompanyRepository.save(previousCompany);
        });
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        return CompanyMapper.toResponse(jpaCompanyRepository.save(CompanyMapper.toEntity(companyRequest)));
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new).getEmployees();
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
